`

## Gitlab Runner CI/CD
 
#### 1. Install Gitlab Runner
```
#curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_<arch>.deb

uname -a

curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
sudo dpkg -i gitlab-runner_amd64.deb

#check success

sudo gitlab-runner
```
Reference: https://docs.gitlab.com/runner/install/linux-manually.html

##### 1.1
To register the runner with the tags of environment
```
sudo gitlab-runner register

# show the result
sudo cat /etc/gitlab-runner/config.toml
```
Refererence: https://docs.gitlab.com/runner/register/

##### 1.2
Add docker run permission to gitlab-runner
```
$ sudo usermod -aG docker gitlab-runner
$ sudo service docker restart
```

##### 1.3 
Create deploy token to deploy with name gitlab-deploy-token.
Reference: https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html#gitlab-deploy-token

##### 1.4

Using Gitlab Runner in docker
```

[[runners]]
  name = ""
  url = "https://gitlab.com/"
  token = ""
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
  [runners.docker]
    tls_verify = false
    image = "docker"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
```
Run docker
```
docker run -d --name gitlab-runner --restart always \
     -v /tmp/config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     gitlab/gitlab-runner:latest
```
Reference: https://docs.gitlab.com/runner/install/docker.html

##### 1.5 commit and push a image version
```
git commit ...
git tag -a v0.3 
git push remote branch --tags
```